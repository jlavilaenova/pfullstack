var Jorge = {
  nombre: 'Jorge',
  apellido: 'Ramirez',
  edad: 35
}

var Esteban = {
  nombre: 'Esteban',
  apellido: 'Ramirez',
  edad: 34
}

var Valeria = {
  nombre: 'Valeria',
  apellido: 'Lopez',
  edad: 12
}



function imprimemayus(persona){
  //var nombre = persona.nombre
  var { nombre } = persona
  console.log(nombre.toUpperCase())
}

//function imprimemayus(persona){
  //console.log(persona.nombre.toUpperCase());
//}

//function imprimemayus({ nombre }){
  //console.log(nombre.toUpperCase());
//}


imprimemayus(Jorge)
imprimemayus(Esteban)
imprimemayus({ nombre: 'Arturo' })



function imprimeNombreEdad( persona ) {
  //Hola, me llamo Jorge y tengo 35 años
  //Hola, me llamo Esteban y tengo 34 años
  var { nombre , edad } = persona
  console.log(`Hola, me llamo ${nombre} y tengo ${edad} años`);
}

imprimeNombreEdad(Jorge)
imprimeNombreEdad(Esteban)

function cumple(persona){
  persona.edad += 1
}



const MAYORIA_DE_EDAD = 18

function validarmayoriaedad(persona){
  //var { nombre , edad } = persona
 return persona.edad >= MAYORIA_DE_EDAD
}

function imprimirvalidacionedad(persona){
  if validarmayoriaedad(persona)){
  console.log(`${persona.nombre} es mayor de edad`);
  }
  else {
    console.log(`${persona.nombre} es menor de edad`);
  }

}

//validarmayoriaedad(Jorge)
//validarmayoriaedad(Esteban)
//validarmayoriaedad(Valeria)
