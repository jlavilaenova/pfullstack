var Jorge = {
  nombre: 'Jorge',
  apellido: 'Ramirez',
  edad: 35
}

var Esteban = {
  nombre: 'Esteban',
  apellido: 'Ramirez',
  edad: 34
}

function imprimemayus(persona){
  //var nombre = persona.nombre
  var { nombre } = persona
  console.log(nombre.toUpperCase())
}

//function imprimemayus(persona){
  //console.log(persona.nombre.toUpperCase());
//}

//function imprimemayus({ nombre }){
  //console.log(nombre.toUpperCase());
//}


imprimemayus(Jorge)
imprimemayus(Esteban)
imprimemayus({ nombre: 'Arturo' })



function imprimeNombreEdad( persona ) {
  //Hola, me llamo Jorge y tengo 35 años
  //Hola, me llamo Esteban y tengo 34 años
  var { nombre , edad } = persona
  console.log(`Hola, me llamo ${nombre} y tengo ${edad} años`);
}

imprimeNombreEdad(Jorge)
imprimeNombreEdad(Esteban)
